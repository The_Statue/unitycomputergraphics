using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateTexture : MonoBehaviour
{
    public TownManager manager;

    [Header("Compute shaders")]
    public ComputeShader computeVoronoi;
    public ComputeShader computeBorders;
    public ComputeShader computeBolden;
    public ComputeShader computeRoads;
    public ComputeShader computeBlur;
    public ComputeShader computeTexturedRoads;
    public ComputeShader computeCombine;

    [Header("Rendered output textures")]
    public RenderTexture renderedVoronoi;
    public RenderTexture renderedBoarders;

    public RenderTexture renderedOutput;

    public RenderTexture renderedOne;
    public RenderTexture renderedRoad;

    public Material material;

    //const int pointsCount = 100;
    //Color[] pointsColor = new Color[pointsCount];
    //Vector2[] pointsPosition = new Vector2[pointsCount];

    [InspectorButton("generatePoints", ButtonWidth = 120)]
    public bool newPoints;
    [InspectorButton("ReRenderImage", ButtonWidth = 120)]
    public bool render;
    [InspectorButton("ChangeMethod", ButtonWidth = 120)]
    public bool cycleMethod;
    [InspectorButton("ChangeDisplay", ButtonWidth = 120)]
    public bool cycleDisplay;

    //Keep square, maintain ratio in townManager
    const int width = 4096; //4096 2048 1024 512
    const int height = 4096;

    Color[] borderRelations = new Color[4194304]; // number of pixels
    Color[] borderColors = new Color[4194304]; // number of pixels


    Color[] teamColors = new Color[100]; // highest possible amount of teams? 100?

    int method = 0;
    bool display = true; //toggle between team display or individual town display
    bool bordersDefined = true; //Only define border relations ONCE

    public int blurAmount = 50;

    public bool drawRoads = true;
    public bool drawBlur = true;
    public bool drawGrass = true;

    public bool drawBolden = true;
    public bool drawTeams = true;

    void Update()
    {
        //for (int i = 0; i < pointsCount; i++)
        //{
        //pointsPosition[i] += new Vector2((Random.value * 2) - 1, (Random.value * 2) - 1);
        //}
        RenderImage(); //does not matter if called early due to function not running until points generated and borders are declared undefined
    }

    void Start()
    {
        //generatePoints();
    }

    public void generatePoints()
    {
        manager.generatePoints();


        teamColors[0] = Color.black;
        teamColors[1] = Color.blue;
        teamColors[2] = Color.red;
        for (int i = 3; i < teamColors.Length; i++)
        {
            teamColors[i] = Random.ColorHSV(0, 1, 0.2f, 1, 0.2f, 1);
        }

        //Run immediate switch incase render is not called every update in future
        bordersDefined = false;
        RenderImage();
    }

    void ChangeMethod()
    {
        method++;
        if (method > 2)
            method = 0;

        //Run immediate switch incase render is not called every update in future
        bordersDefined = false;
        RenderImage();
    }

    void ChangeDisplay()
    {
        display = !display;

        //Run immediate switch incase render is not called every update in future
        RenderImage();
    }

    //go through every pixel and define which territories border each other based on which colours touch each other
    void DefineBorders(Color[] colors)
    {
        for (int i = 0; i < borderRelations.Length; i++)
        {
            if (borderRelations[i] != null && colorMatch(borderRelations[i], Color.white))
            {
                //Debug.Log(borderColors[i]);
                //loop through to find town ID
                int town1ID = -1;// itself
                int town2ID = -1;// right
                int town3ID = -1; // below
                int town4ID = -1; // right and below
                for (int j = 0; j < colors.Length; j++)
                {
                    if (town1ID == -1 && colorMatch(borderColors[i], manager.towns[j].color))
                    {
                        //Debug.Log("MatchFound1: " + i + " " + j);
                        town1ID = j;
                    }
                    //stop it wrapping around the board
                    if ((i + 1) % 2048 > 0)
                    {
                        //check directly right
                        if (town2ID == -1 && i + 1 < borderRelations.Length && colorMatch(borderColors[i + 1], manager.towns[j].color))
                        {
                            town2ID = j;
                        }
                        //check below and to the right
                        if (town4ID == -1 && i + width + 1 < borderRelations.Length && colorMatch(borderColors[i + width + 1], manager.towns[j].color))
                        {
                            town4ID = j;
                        }
                    }
                    //check directly below
                    if (town3ID == -1 && i + width < borderRelations.Length && colorMatch(borderColors[i + width], manager.towns[j].color))
                    {
                        town3ID = j;
                    }

                    //if one other found, break out of loop
                    if (town1ID != -1 && (town2ID != -1 || town3ID != -1 || town4ID != -1))
                        break;

                }
                //Start adding neighbours on found borders
                if (town1ID != -1 && town2ID != -1)
                {
                    manager.towns[town1ID].AddNeighbour(manager.towns[town2ID]);
                }
                if (town1ID != -1 && town3ID != -1)
                {
                    manager.towns[town1ID].AddNeighbour(manager.towns[town3ID]);
                }
                if (town1ID != -1 && town4ID != -1)
                {
                    manager.towns[town1ID].AddNeighbour(manager.towns[town4ID]);
                }
            }//    Debug.Log("Line at: " + i);
        }
    }

    void CreateTextures()
    {
        if (renderedVoronoi == null)
        {
            renderedVoronoi = new RenderTexture(width, height, 0);
            renderedVoronoi.enableRandomWrite = true;
            renderedVoronoi.Create();
        }
        if (renderedBoarders == null)
        {
            renderedBoarders = new RenderTexture(width, height, 0);
            renderedBoarders.enableRandomWrite = true;
            renderedBoarders.Create();
        }

        //used for vornoi noise
        if (renderedOne == null)
        {
            renderedOne = new RenderTexture(width, height, 0);
            renderedOne.enableRandomWrite = true;
            renderedOne.Create();
        }

        //used for roads
        if (renderedRoad == null || !drawRoads)
        {
            renderedRoad = new RenderTexture(width, height, 0);
            renderedRoad.enableRandomWrite = true;
            renderedRoad.Create();
        }

        if (renderedOutput == null)
        {
            renderedOutput = new RenderTexture(width, height, 0);
            renderedOutput.enableRandomWrite = true;
            renderedOutput.Create();
        }
    }

    RenderTexture VoronoiNoise(float[] resolution, float[] positions, float[] colors)
    {
        RenderTexture texture = new RenderTexture((int)(resolution[0]), (int)(resolution[1]), 0);
        texture.enableRandomWrite = true;
        texture.Create();

        //send them to the compute shader
        computeVoronoi.SetTexture(0, "Result", texture);
        computeVoronoi.SetFloats("Resolution", resolution);

        computeVoronoi.SetFloats("Points", positions);
        computeVoronoi.SetFloats("Colors", colors);

        computeVoronoi.SetInt("Method", method);
        computeVoronoi.Dispatch(0, (int)(resolution[0]) / 8, (int)(resolution[1]) / 8, 1);

        return texture;
    }
    RenderTexture HighlightBorders(RenderTexture input, float[] resolution)
    {
        RenderTexture texture = new RenderTexture((int)(resolution[0]), (int)(resolution[1]), 0);
        texture.enableRandomWrite = true;
        texture.Create();

        computeBorders.SetTexture(0, "Result", texture);

        computeBorders.SetTexture(0, "mainTexture", input);

        computeBorders.Dispatch(0, (int)(resolution[0]) / 8, (int)(resolution[1]) / 8, 1);

        return texture;
    }
    RenderTexture BoldenBorders(RenderTexture input, float[] resolution)
    {
        RenderTexture texture = new RenderTexture((int)(resolution[0]), (int)(resolution[1]), 0);
        texture.enableRandomWrite = true;
        texture.Create();

        //bolden borders
        RenderTexture renderedBoldTempTexture = new RenderTexture(input); //clone the variable to the temp
        Graphics.CopyTexture(input, renderedBoldTempTexture);

        for (int i = 0; i < 5; i++) //set amount of boldness (should be amount of pixels added to line?)
        {
            computeBolden.SetTexture(0, "Border", renderedBoldTempTexture);
            computeBolden.SetTexture(0, "Result", texture);

            computeBolden.Dispatch(0, (int)(resolution[0]) / 8, (int)(resolution[1]) / 8, 1);
            Graphics.CopyTexture(texture, renderedBoldTempTexture); //update with result for loop
        }
        return texture;
    }
    RenderTexture DrawRoads(float[] resolution, float[] positions)
    {
        RenderTexture texture = new RenderTexture((int)(resolution[0]), (int)(resolution[1]), 0);
        texture.enableRandomWrite = true;
        texture.Create();

        computeRoads.SetTexture(0, "Result", texture);
        computeRoads.SetFloats("Resolution", resolution);
        computeRoads.SetFloats("Points", positions);

        computeRoads.Dispatch(0, (int)(resolution[0]) / 8, (int)(resolution[1]) / 8, 1);

        return texture;
    }
    RenderTexture Blur(RenderTexture input, float loopCount, float[] resolution, float[] positions)
    {
        RenderTexture texture = new RenderTexture(width, height, 0);
        texture.enableRandomWrite = true;
        texture.Create();

        RenderTexture renderedBlurTempTexture = new RenderTexture(input);
        Graphics.CopyTexture(input, renderedBlurTempTexture); //update with result for loops
        for (int i = 0; i < loopCount; i++)
        {
            computeBlur.SetTexture(0, "Result", texture);
            computeBlur.SetTexture(0, "Input", renderedBlurTempTexture);

            computeBlur.Dispatch(0, (int)(resolution[0]) / 8, (int)(resolution[1]) / 8, 1);
            Graphics.CopyTexture(texture, renderedBlurTempTexture); //update with result for loop
        }

        return texture;
    }
    RenderTexture TextureRoad(RenderTexture input, float[] resolution)
    {
        RenderTexture texture = new RenderTexture((int)(resolution[0]), (int)(resolution[1]), 0);
        texture.enableRandomWrite = true;
        texture.Create();

        computeTexturedRoads.SetTexture(0, "Result", texture);
        computeTexturedRoads.SetTexture(0, "Input", input);
        computeTexturedRoads.SetFloats("Resolution", resolution);

        computeTexturedRoads.Dispatch(0, (int)(resolution[0]) / 8, (int)(resolution[1]) / 8, 1);

        return texture;
    }
    RenderTexture Combine(RenderTexture inputOne, RenderTexture inputTwo, float[] resolution)
    {
        RenderTexture texture = new RenderTexture((int)(resolution[0]), (int)(resolution[1]), 0);
        texture.enableRandomWrite = true;
        texture.Create();

        computeCombine.SetTexture(0, "Result", texture);
        computeCombine.SetTexture(0, "MainImage", inputOne);//renderedTexture
        computeCombine.SetTexture(0, "MaskImage", inputTwo);

        computeCombine.Dispatch(0, (int)(resolution[0]) / 8, (int)(resolution[1]) / 8, 1);

        return texture;
    }

    public void ReRenderImage()
    {
        bordersDefined = false;
    }

    void RenderImage()
    {
        if (manager.redraw || !bordersDefined) //should be false until first draw called by user
        {
            CreateTextures();

            //define resolution to be passed in
            float[] resolution = { width, height, 0, 0 };


            //Get position and colours from manager
            Vector2[] positions = new Vector2[manager.towns.Length];
            Color[] colors = new Color[manager.towns.Length];
            Color[] teams = new Color[manager.towns.Length];
            for (int i = 0; i < manager.towns.Length; i++)
            {
                positions[i] = new Vector2(manager.towns[i].position.x * width, manager.towns[i].position.y * height); //multiply by size of texture(currently assuming square image)
                colors[i] = manager.towns[i].color;
                teams[i] = teamColors[manager.towns[i].teamID];
            }

            float[] positionsFloat = convertToFloats(positions);
            float[] colorsFloat = convertToFloats(colors);

            if (!bordersDefined)
            {
                //Required voronoi noise to determine individual towns
                renderedVoronoi = VoronoiNoise(resolution, positionsFloat, colorsFloat);
                //Required voronoi noise to determine neighbours
                renderedBoarders = HighlightBorders(renderedVoronoi, resolution);
                if (drawBolden)
                    renderedOne = BoldenBorders(renderedBoarders, resolution);
                else
                    renderedOne = renderedBoarders;



                //!!Required CPU component to calculate neighbours!!
                Texture2D texture1 = toTexture2D(renderedBoarders); // get array for pixel proccessing, comparing teritory colour TODO: Determin if there is a threat of random assigning two of the exact colour
                borderRelations = texture1.GetPixels();

                //get second array of colours
                Texture2D texture2 = toTexture2D(renderedVoronoi); // get array for pixel proccessing, comparing teritory colour 
                borderColors = texture2.GetPixels(); //material.GetTexture("_BaseColorMap").GetPixels();

                DefineBorders(colors);


                if (drawRoads)
                    renderedRoad = DrawRoads(resolution, positionsFloat);
                if (drawBlur)
                    renderedRoad = Blur(renderedRoad, blurAmount, resolution, positionsFloat);
                if (drawGrass)
                    renderedRoad = TextureRoad(renderedRoad, resolution);
            }


            if (drawTeams) //redraw map using team colours if asked
                renderedVoronoi = VoronoiNoise(resolution, positionsFloat, convertToFloats(teams));

            //Combine both textures into final output, with both colour and borders
            renderedOutput = Combine(renderedRoad, renderedVoronoi, resolution);
            renderedOutput = Combine(renderedOutput, renderedOne, resolution);

            //Send final texture
            material.SetTexture("_BaseColorMap", renderedOutput);

            bordersDefined = true;
            manager.redraw = false; //do not need to draw next frame as no changes are made
        }
    }
    Texture2D toTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // ReadPixels looks at the active RenderTexture.
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }

    bool colorMatch(Color one, Color two)
    {
        const float deviationAllowed = 0.003f;
        bool r = false;
        bool g = false;
        bool b = false;

        if (one.r >= two.r - deviationAllowed && one.r <= two.r + deviationAllowed)
            r = true;
        if (one.g >= two.g - deviationAllowed && one.g <= two.g + deviationAllowed)
            g = true;
        if (one.b >= two.b - deviationAllowed && one.b <= two.b + deviationAllowed)
            b = true;

        return r && g && b;
    }


    float[] convertToFloats(Vector2[] array)
    {
        float[] result = new float[array.Length * 4];

        for (int i = 0; i < array.Length; i++)
        {
            result[i * 4] = array[i].x;
            result[(i * 4) + 1] = array[i].y;
            result[(i * 4) + 2] = 0;
            result[(i * 4) + 3] = 0;
        }

        return result;
    }
    float[] convertToFloats(Vector2 vec)
    {
        float[] result = new float[4];

        result[0] = vec.x;
        result[1] = vec.y;
        result[2] = 0;
        result[3] = 0;

        return result;
    }

    float[] convertToFloats(Vector4[] array)
    {
        float[] result = new float[array.Length * 4];

        for (int i = 0; i < array.Length; i++)
        {
            result[i * 4] = array[i].x;
            result[(i * 4) + 1] = array[i].y;
            result[(i * 4) + 2] = array[i].z;
            result[(i * 4) + 3] = array[i].w;
        }

        return result;
    }

    float[] convertToFloats(Color[] array)
    {
        float[] result = new float[array.Length * 4];

        for (int i = 0; i < array.Length; i++)
        {
            result[i * 4] = array[i].r;
            result[(i * 4) + 1] = array[i].g;
            result[(i * 4) + 2] = array[i].b;
            result[(i * 4) + 3] = array[i].a;
        }

        return result;
    }

}
