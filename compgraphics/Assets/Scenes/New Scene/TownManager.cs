using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownManager : MonoBehaviour
{

    public class Town
    {

        public Town(Vector2 townPos, Color townColor, string townName, string townDescription, GameObject townObject, int units, int team = 0)
        {
            position = townPos;
            color = townColor;

            name = townName;
            description = townDescription;
            townObj = townObject;
            Units = units;
            //neighbours = new List<Town>();

            script = townObj.GetComponent<TownObject>();
            AddToTeam(team);
        }
        public string name;
        public string description;

        public Vector2 position;// = new Vector2[pointsCount];
        public Color color;// = new Color[pointsCount];
        public GameObject townObj;
        TownObject script;

        public List<Town> neighbours = new List<Town>();
        public int teamID;
        public int Units = 100;

        public int unitsThisTurn = 0;

        //used to tell this town that its turn just started
        public void DeclareTurn()
        {
            Units += 5; //TODO: change additional units to be dynamic based on town quality or units within the town at start of turn?

            DeclareStage(); //TODO: may be redudant
        }
        //used to tell this town that a new stage occured (essentially to reupdate temp units)
        public void DeclareStage()
        {
            unitsThisTurn = Units;
        }

        public void AddNeighbour(Town neighbour)
        {
            //if relationship already exists, ignore AND check if not self
            if (!ExistingNeighbour(neighbour) && neighbour.name != name)
            {
                neighbours.Add(neighbour);
                neighbour.AddNeighbour(this);
            }
        }
        public bool ExistingNeighbour(Town neighbour)
        {
            if (neighbours.Contains(neighbour))
            {
                return true;
            }
            return false;
        }

        public void UseTurn(Town otherTown, float percentage)
        {
            //TODO: maybe check if current turn?
            SummonSoldiers(otherTown, percentage);
        }
        bool SummonSoldiers(Town target, float percentage)
        {
            //Ensure that the attempted attack is legal
            if (neighbours.Contains(target))
            {
                // this script uses temp units to avoid people chaining units to the front lines through towns, they can only move units from a town if they were in that town at the start of the round
                int units = (int)Mathf.Round(unitsThisTurn * percentage); // derive how many units to spawn (using temp units only)

                for (int i = 0; i < units; i++) // replace with percentage selector for unit amount
                {
                    townObj.GetComponent<SummonSoldier>().Summon((i % rowSize) - (rowSize) / 2, (i / rowSize) * 2.5f, this, target);
                    Units--; //consume the units as the attack force
                    unitsThisTurn--; //mimic on the temp units 
                }
                return true;
            }
            return false;
        }

        public bool Attack(Town defender, int attackForce = 1)
        {
            //Ensure that the attempted attack is legal
            if (neighbours.Contains(defender))
            {
                //Units -= units; //consume the units as the attack force

                if (defender.teamID == teamID && teamID != 0)
                {
                    return Deploy(defender, attackForce);
                }

                //calculate if the enemy can defend, if not, continue
                if (!defender.Defend(this, attackForce))
                {
                    //figure out a way to assign new ID if no team formed
                    if (teamID == 0)
                        AddToTeam();

                    defender.AddToTeam(teamID); //convert to our ID

                    return true;
                }
            }
            return false;
        }
        //private so it doesnt have to check if legal attack again
        bool Defend(Town attacker, int attackForce = 1)
        {
            //TODO: change percentage of attack force from town
            //int attackForce = units;

            //Check if 1 to 1 unit annihilation, if attacker has remaining units(meaning more units initally) attack is successful
            if (attackForce - Units >= 0)
            {
                Units = attackForce - Units; //deploy attack force as the new defence force for the captured town
                return false;
                //AddToTeam(attacker.teamID);
            }
            else
            {
                Units = Units - attackForce; //deminish the defending units by casulties
                return true;
            }
        }
        //private so it doesnt have to check if legal attack again
        bool Deploy(Town reciever, int attackForce = 1)
        {
            reciever.Units += attackForce;
            return true;
        }


        public void AddToTeam(int ID) //Add town to specific team, adjusting global counter
        {
            //Debug.Log("adding at Location: " + ID);
            if (teamID != 0)
                TownManager.instance.townTeams[teamID]--;
            if (ID != 0)
                TownManager.instance.townTeams[ID]++;
            teamID = ID;
            TownManager.instance.SetRedraw(0); //0 delay
        }

        public void AddToTeam() //search for first team that has no existing towns
        {
            for (int i = 1; i < TownManager.instance.townTeams.Length; i++)
            {
                Debug.Log("Testing Location: " + i);
                if (TownManager.instance.townTeams[i] == 0)
                {
                    AddToTeam(i);
                    break;
                }
            }
        }
        public void ToggleUI(bool value)
        {
            if (script.UIObj != null)
                script.UIObj.SetActive(value);
        }
    }

    public static TownManager instance;

    public Town[] towns = new Town[pointsCount];
    public int[] townTeams = new int[pointsCount]; //Teams

    public TurnManager turnManager;
    public GameObject townPrefab;
    const int pointsCount = 100;

    public TextAsset townNamesDoc;
    string[] townNames;

    public int width = 1;
    public int height = 1;

    const int rowSize = 5;

    public bool redraw = false; //used to tell the generateTexture script to redraw the coloured component of the texture

    private void Awake()
    {
        if (instance != null)
            Debug.LogError("We already have a TownManager!");

        instance = this;
    }

    void Start()
    {
        //Get generateTexture to call generatePoints first
        //generatePoints();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public Vector3 transformPosition(Vector3 pos)
    {
        return new Vector3((pos.x - 0.5f) * width * 1000.0f, 0, (pos.y - 0.5f) * height * 1000.0f);
    }

    public void generatePoints()
    {
        if (townNames == null)
            parseNames();

        bool[] usedNames = new bool[townNames.Length];

        for (int i = 0; i < pointsCount; i++)
        {
            if (towns[i] != null)
            { 
                Destroy(towns[i].townObj);

                int teamID = towns[i].teamID;
                if (townTeams[teamID] > 0)  //reduce the count on each team but ensure it doesnt go negative (mainly all team 0)
                    townTeams[teamID]--;
            }


            Vector2 townPos = new Vector2(Random.value, Random.value); //TODO: try keep away from existing towns? maybe?
            Color townColor = Random.ColorHSV(0, 1, 0.2f, 1, 0.2f, 1);
            string townName = "Town #" + i;
            string townDesc = "Town #" + i;

            if (townNames != null)
            {
                int random = Random.Range(0, townNames.Length / 2) * 2;
                while (usedNames[random]) //keep randomising until an unused name is found TODO: could theoretically crash if all names get used up
                {
                    random = Random.Range(0, townNames.Length / 2) * 2;
                }
                townName = townNames[random]; //only do evens as odds carry the description
                townDesc = townNames[random + 1]; //only do evens as odds carry the description
            }

            GameObject temp = Instantiate(townPrefab, transformPosition(townPos), Quaternion.identity);
            //TODO: get solution to predefining territories(may assume no territories till a town captures another?)
            if (i == 0)
            {
                townName = "Player's Town";
                towns[i] = new Town(townPos, townColor, townName, "Town of beginnings or something.", temp, 100, i + 1);
            }
            else if (i == 1)
            {
                townName = "Enemy's Town";
                towns[i] = new Town(townPos, townColor, townName, "Probably the bad guys.", temp, 100, 2);
            }
            else
                towns[i] = new Town(townPos, townColor, townName, townDesc, temp, (i / 2));
            temp.GetComponent<TownObject>().town = towns[i];

            temp.GetComponent<SummonSoldier>().turnManager = turnManager;
        }
        //RenderImage();
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        for (int i = 0; i < towns.Length; i++)
        {
            if (towns[i] != null)
            {
                for (int j = 0; j < towns[i].neighbours.Count; j++)
                {
                    if (towns[i].neighbours[j] != null)
                    {
                        Vector3 position1 = towns[i].townObj.transform.position;
                        Vector3 position2 = towns[i].neighbours[j].townObj.transform.position;

                        //if (Vector3.Distance(position2, position1) > 300.0f)
                        if (towns[i].teamID == 1)//|| towns[i].neighbours[j].teamID == 1)
                        {
                            Gizmos.DrawLine(position1, position2);
                        }

                    }
                }
            }
        }
    }
    void SetRedraw(float delayTime = 0)
    {
        StartCoroutine(DelayRedraw(delayTime));
    }
    IEnumerator DelayRedraw(float delayTime)
    {
        //Wait for the specified delay time before continuing.
        yield return new WaitForSeconds(delayTime);

        redraw = true;
    }

    void parseNames()
    {
        townNames = townNamesDoc.text.Split('\n');
    }
}
