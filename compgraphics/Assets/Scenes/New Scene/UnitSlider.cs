using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UnitSlider : MonoBehaviour
{
    public Slider slider;
    public TMP_Text sendingText;
    public TMP_Text remainingText;

    public TownSelector selector;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (selector.selectedTown != null)
        {
            sendingText.text = "Units to send:\n" + (int)Mathf.Round(selector.selectedTown.unitsThisTurn * slider.value);
            remainingText.text = "Units to stay:\n" + (selector.selectedTown.Units - (int)Mathf.Round(selector.selectedTown.unitsThisTurn * slider.value));
        }
        else
        {
            sendingText.text = "Select a town!";
            remainingText.text = "";
        }
    }
}
