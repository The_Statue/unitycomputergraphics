using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TownSelector : MonoBehaviour
{
    public TownManager manager;
    public TownDescriber desc;
    public TurnManager turn;
    public GenerateArrow arrow;
    public UnitSlider Slider;
    Camera cam;
    public Vector3 position = Vector3.zero;
    Vector3 defaultPosition;
    Vector3 zoomedPosition = new Vector3(0, 450, -125);

    public TownManager.Town selectedTown;
    public TownManager.Town hoveredTown;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        defaultPosition = cam.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 2000) && EventSystem.current != null && EventSystem.current.IsPointerOverGameObject() == false) //IsPointerOverGameObject requires 0 as argument if in android build
        {
            position = hit.point;
            int closestTownID = 0;
            //find closest town to mouse
            for (int i = 0; i < manager.towns.Length; i++)
            {
                if (manager.towns[i] != null && Vector3.Distance(manager.towns[i].townObj.transform.position, position) < Vector3.Distance(manager.towns[closestTownID].townObj.transform.position, position))
                {
                    closestTownID = i;
                }
            }

            //manage select and hover targets
            if (manager.towns[closestTownID] != null)
            {


                if (hoveredTown == null || (hoveredTown != manager.towns[closestTownID]))
                {
                    //reset UI before switching hover target
                    if (hoveredTown != null)
                        hoveredTown.ToggleUI(false);

                    hoveredTown = manager.towns[closestTownID];
                    hoveredTown.ToggleUI(true);
                    position = hoveredTown.townObj.transform.position;

                    SelectTown();
                }

                //if left click
                if (Input.GetMouseButtonDown(0))
                {
                    //if already selected something, and the target is in range
                    if (selectedTown != null && selectedTown.teamID == turn.CurrentTurn() && selectedTown.ExistingNeighbour(hoveredTown) && /*check if in prep phase and is same team*/(((selectedTown.teamID == hoveredTown.teamID) && (turn.CurrentStage() == 1)) || (turn.CurrentStage() != 1)) && /*check if in attack phase and not same team*/(((selectedTown.teamID != hoveredTown.teamID) && (turn.CurrentStage() == 2)) || (turn.CurrentStage() != 2)) && /*check if in redeploy phase and is same team*/(((selectedTown.teamID == hoveredTown.teamID) && (turn.CurrentStage() == 3)) || (turn.CurrentStage() != 3)))
                    {
                        selectedTown.UseTurn(hoveredTown, Slider.slider.value);
                        //turn.IncrementTurn();

                        //selectedTown.townObj.GetComponent<TownObject>().UIEnabled = false;
                        selectedTown.ToggleUI(false);
                        selectedTown = null;
                    }
                    else
                    {
                        if (selectedTown != null)
                            selectedTown.ToggleUI(false);
                        selectedTown = hoveredTown;

                        desc.selected(selectedTown); //add the selected town as the currently looked at town

                        //Debug.Log("TownSelected: " + selectedTown.name);
                        cam.transform.position = new Vector3(selectedTown.townObj.transform.position.x, 0, selectedTown.townObj.transform.position.z) + zoomedPosition;
                    }
                }
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            UnselectTown();
        }


    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(position, Vector3.one * 20);
        if (selectedTown != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawCube(selectedTown.townObj.transform.position, Vector3.one * 20);
        }
    }

    void SelectTown()
    {
        if (selectedTown != null && selectedTown.townObj != null)
        {
            arrow.targetOne = selectedTown.townObj.transform.position;
            arrow.targetTwo = position;
        }
        arrow.RegenMesh();
    }
    void UnselectTown()
    {
        if (selectedTown != null)
        {
            selectedTown.ToggleUI(false);
            selectedTown = null;
        }
        cam.transform.position = defaultPosition;//new Vector3(0, 745, -375);

        arrow.targetOne = Vector3.zero;
        arrow.targetTwo = Vector3.zero;
        arrow.RegenMesh();

        desc.selected(null);
    }
}
