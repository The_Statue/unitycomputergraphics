using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownModeller : MonoBehaviour
{
    public GameObject[] buildings;
    TownObject townObj;
    // Start is called before the first frame update
    void Start()
    {
        townObj = gameObject.GetComponent<TownObject>();
        GameObject[] lilBuildings = new GameObject[(int)Mathf.Ceil(townObj.town.Units / 2.0f)]; //defines how many buildins there are


        for (int i = 0; i < lilBuildings.Length; i++)
        {
            GameObject prefab = buildings[Random.Range(0, buildings.Length)];

            bool intersected = false;
            Vector3 pos = new Vector3(Random.Range(-25, 25), 0, Random.Range(-25, 25));
            for (int l = 0; l < 1000/*limit to prevent infinite loop*/; l++)
            {
                pos = new Vector3(Random.Range(-25, 25), prefab.transform.localScale.y / 2, Random.Range(-25, 25)); //attempt to generate a random position
                intersected = false;

                //loop through all already generated buildings
                for (int j = 0; j < i; j++)
                {
                    if (Vector3.Distance(pos, lilBuildings[j].transform.position) < /*get the minimum distance between the two objects based on their sizes*/(prefab.transform.localScale.x + lilBuildings[j].transform.localScale.x)) //TODO: find a system to get the size better?
                    {
                        //the two buildings are intersected
                        intersected = true;
                    }
                }
                if (intersected == false)
                    break;
            }
            pos += gameObject.transform.position;
            lilBuildings[i] = Instantiate(prefab, pos, Quaternion.Euler(0, Random.Range(-180, 180), 0), gameObject.transform);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
