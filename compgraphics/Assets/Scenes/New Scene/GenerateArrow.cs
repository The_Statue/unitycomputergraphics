using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateArrow : MonoBehaviour
{
    public Vector3 targetOne;
    public Vector3 targetTwo;


    public float width = 1;
    public float height = 1;

    const int vertCount = 26; //must be even and atleast 4

    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    Mesh mesh;

    public Material material;

    public AnimationCurve pointHeight;

    public void Start()
    {
        //initalise variables
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        meshRenderer.sharedMaterial = material;

        meshFilter = gameObject.AddComponent<MeshFilter>();

        //initialise mesh
        GenerateNewMesh();
    }

    private void Update()
    {
        //RegenMesh();
    }

    public void RegenMesh()
    {
        mesh = meshFilter.mesh;
        Vector3 targetOneTemp = targetOne; // remove y from target vector before its calculated TODO: find more elegant way to handle
        Vector3 targetTwoTemp = targetTwo;
        targetOneTemp.y = 0;
        targetTwoTemp.y = 0;

        Vector3 targetVector = Vector3.Normalize(targetTwoTemp - targetOneTemp);

        Vector3[] vertices = new Vector3[vertCount];
        for (int i = 0; i < vertCount - 1; i += 2)
        {
            float percent = (float)i / (vertCount - 2.0f);
            Vector3 height = new Vector3(0, pointHeight.Evaluate(percent), 0);

            Vector3 pos = Vector3.Lerp(targetOne, targetTwo, percent);
            vertices[i] = pos + Quaternion.Euler(0, 90, 0) * (targetVector * 10) + height;
            vertices[i + 1] = pos + Quaternion.Euler(0, 90, 0) * (targetVector * -10) + height;
        }

        //vertices[2] = targetTwo + (Quaternion.Euler(0, 90, 0) * targetVector) * 5;
        //vertices[3] = targetTwo + (Quaternion.Euler(0, 90, 0) * targetVector) * -5;

        mesh.vertices = vertices;

        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();


        meshFilter.mesh = mesh;
    }

    void GenerateNewMesh()
    {
        mesh = new Mesh();

        Vector3 targetVector = Vector3.Normalize(targetTwo - targetOne);

        Vector3[] vertices = new Vector3[vertCount];

        for (int i = 0; i < vertCount; i++)
            vertices[i] = Vector3.one;

        mesh.vertices = vertices;

        int[] tris = new int[(vertCount - 2) * 3];
        for (int i = 0; i < (vertCount - 2) * 3; i += 3)
        {
            // lower left triangle
            if ((i / 3) % 2 == 0)
            {
                tris[i] = (i / 3);
                tris[i + 1] = (i / 3) + 1;
                tris[i + 2] = (i / 3) + 2;
            }
            else
            {
                tris[i] = (i / 3);
                tris[i + 1] = (i / 3) + 2;
                tris[i + 2] = (i / 3) + 1;
            }
        }
        mesh.triangles = tris;

        Vector3[] normals = new Vector3[vertCount];

        for (int i = 0; i < vertCount; i++)
            normals[i] = -Vector3.forward;

        mesh.normals = normals;

        Vector2[] uv = new Vector2[vertCount];
        for (int i = 0; i < vertCount - 1; i += 2)
        {
            if (i == vertCount - 2) // the end of the head
            {
                uv[i] = new Vector2(1, 0);
                uv[i + 1] = new Vector2(1, 1);
            }
            else if (i == vertCount - 4) //the start of the head
            {
                uv[i] = new Vector2(0.8f, 0);
                uv[i + 1] = new Vector2(0.8f, 1);
            }
            else //do the tail of the arrow
            {
                uv[i] = new Vector2(0.5f, 0);
                uv[i + 1] = new Vector2(0.5f, 1);
            }
        }
        mesh.uv = uv;

        mesh.MarkDynamic();

        meshFilter.mesh = mesh;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(targetOne + gameObject.transform.position, Vector3.one * 5);
        Gizmos.DrawCube(targetTwo + gameObject.transform.position, Vector3.one * 5);

    }
}
