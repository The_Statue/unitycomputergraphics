using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonSoldier : MonoBehaviour
{
    public GameObject prefabSoldier;

    [Header("Will be declared by townmanager when spawned")]
    public TurnManager turnManager;
    const float size = 15;
    const int rowSize = 5; //keep consistent with TownManager.cs
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Summon(float offset, float delay, TownManager.Town home, TownManager.Town target)
    {
        StartCoroutine(DelaySummon(offset, delay + Random.Range(0,0.5f), home, target));
    }
    IEnumerator DelaySummon(float offset, float delayTime, TownManager.Town home, TownManager.Town target)
    {
        //Wait for the specified delay time before continuing.
        yield return new WaitForSeconds(delayTime);

        //Do the action after the delay time has finished.
        Vector3 townPos = gameObject.transform.position;
        Vector3 targetPos = target.townObj.transform.position;
        Vector3 offsetPos = ((Quaternion.Euler(0, 90, 0) * (targetPos - townPos).normalized)) * offset * size;

        //townPos = townPos + offsetPos; //offset spawn position

        GameObject Soldier = Instantiate(prefabSoldier, townPos + offsetPos, Quaternion.LookRotation(targetPos - townPos, Vector3.up));

        Soldier.GetComponent<HandleSoldier>().targetPos = targetPos + offsetPos;
        Soldier.GetComponent<HandleSoldier>().sizeTarget = Vector3.one * size;
        Soldier.GetComponent<HandleSoldier>().home = home;
        Soldier.GetComponent<HandleSoldier>().target = target;
        Soldier.GetComponent<HandleSoldier>().turn = turnManager;
    }
}
