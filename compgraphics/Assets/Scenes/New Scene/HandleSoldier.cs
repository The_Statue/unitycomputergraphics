using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleSoldier : MonoBehaviour
{
    public Animator anim;
    public GameObject AttackFX;
    public GameObject DeathFX;
    public GameObject SupportFX;
    Vector3 startPosition;

    float timeElapse = 0;
    float distance = 0;
    const float speed = 15f;

    public Vector3 sizeTarget;

    public AnimationCurve spawnCurve;
    public AnimationCurve moveCurve;
    public AnimationCurve removeCurve;


    public TownManager.Town target;
    public TownManager.Town home;
    public Vector3 targetPos;

    public TurnManager turn;

    Vector3 lastFramePos;

    int state = 0; // 0 is initial, 1 is walking, 2 is attacking, 3 is removing

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        distance = Vector3.Magnitude(startPosition - targetPos);

        gameObject.transform.localScale = Vector3.one;

        lastFramePos = startPosition;

        turn.activeUnits++; //declare yourself to the turn manager so it can keep track of game pieces pending
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case 0:
                //spawn from our town
                gameObject.transform.localScale = spawnCurve.Evaluate(timeElapse) * sizeTarget;

                if (timeElapse > 1) //full size
                    IncreaseState();
                timeElapse += Time.deltaTime;
                break;
            case 1:
                //move to other town
                float xP = (timeElapse / distance) * speed;
                float tP = moveCurve.Evaluate(xP);
                gameObject.transform.position = Vector3.Lerp(startPosition, targetPos, tP);
                anim.SetFloat("Forward", 0.75f); //start walking

                if (xP > 1) //Reached town
                    IncreaseState();
                timeElapse += Time.deltaTime;
                break;
            case 2:
                //attack the other town TODO: check if friendly town
                float tA = (timeElapse) / 2.0f;
                //gameObject.transform.localScale = Vector3.Lerp(Vector3.one, sizeTarget, tA);
                anim.SetFloat("Forward", 0); //stop walking

                if (timeElapse > 1) //full size
                {
                    IncreaseState();

                    if (target != null && home != null && (target.teamID != home.teamID || target.teamID == 0)) //ensure valid for attack FX
                    {
                        //spawn attack FX
                        GameObject Attack = Instantiate(AttackFX, gameObject.transform.position + new Vector3(0, 25, 0), Quaternion.Euler(Vector3.right * 90));
                        Attack.transform.localScale = sizeTarget * 1.5f;
                    }
                }
                timeElapse += Time.deltaTime;
                break;
            case 3:
                //remove character
                float tR = removeCurve.Evaluate(timeElapse);
                gameObject.transform.localScale = Vector3.Lerp(sizeTarget, Vector3.one, tR);

                if (timeElapse > 1) //full size
                    IncreaseState();
                timeElapse += Time.deltaTime;
                break;
            default:
                GameObject exp;
                if (target != null && home != null && (target.teamID == home.teamID && target.teamID != 0))
                    exp = Instantiate(SupportFX, gameObject.transform.position + new Vector3(0, 25, 0), Quaternion.Euler(Vector3.right * -90));
                else
                    exp = Instantiate(DeathFX, gameObject.transform.position, Quaternion.Euler(Vector3.right * 90));

                exp.transform.localScale = sizeTarget * 1.75f;

                home.Attack(target); //do the actual attack after all the animation is completed

                turn.activeUnits--; //remove yourself from the active game pieces in the turn manager

                Destroy(gameObject);
                break;
        }
    }

    void IncreaseState()
    {
        state++;
        timeElapse = 0;
    }
}
