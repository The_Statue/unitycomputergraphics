using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TurnManager : MonoBehaviour
{
    public int activeUnits = 0; // only continue the turn if no units are active
    public int turn = 0; //start on player one (0 is neutral)
    public int stage = 1; // 1 is prep, 2 is attack, 3 is redeploy
    public TownManager manager;

    public TMP_Text text;
    public Image slider;

    int winner = 0;

    float barTimer = 0;
    // Start is called before the first frame update
    void Start()
    {
        //IncrementTurn(); //called in Customise UI when player presses redraw for first time
    }

    // Update is called once per frame
    void Update()
    {
        float percent = Mathf.Lerp((stage - 1) / 3.0f, stage / 3.0f, barTimer);

        if (winner != 0)
        {
            percent = 1;
        }
        slider.fillAmount = percent;
        barTimer += Time.deltaTime;
    }

    public void IncrementTurn()
    {
        if (manager.towns[0] == null) // just check if the first item is declared, otherwise assume none are declared
            return;

        int prevTurn = turn;
        for (int i = 0; i < /*replaced a while loop with as many towns exist*/100; i++)
        {
            if (manager.townTeams[++turn] != 0) //find the first non empty team
                break;

            if (turn >= manager.townTeams.Length - 1)
                turn = 0;
        }; //if team has no members in it, skip it

        if(turn == prevTurn)
        {
            //Game END
            turn = 0;
            stage = 0;
            winner = prevTurn; //save winner :)
        }

        for (int i = 0; i < manager.towns.Length; i++)
        {
            if ( manager.towns[i].teamID == turn) // loop through all towns and check if this town is apart of the team who is currently active
            {
                manager.towns[i].DeclareTurn();
            }
            else
                manager.towns[i].unitsThisTurn = 0; //maybe add a declare not turn function if needs increase?
        }
        if (turn != 0 && stage != 0)
            text.text = "Turn: team #" + CurrentTurn();
        else //if game won
            text.text = "Game over! Winner, Team #" + winner;
    }

    public int CurrentTurn()
    {
        return turn;
    }


    public void IncrementStage()
    {
        if (stage == 0) //if out of stage bounds, ignore (essentially game over)
            return;

        if (activeUnits > 0) //dont allow units on the game board while changing stages
            return;

        if (++stage > 3)
        {
            stage = 1;
            IncrementTurn();
        }
        else
        {
            for (int i = 0; i < manager.towns.Length; i++)
            {
                if (manager.towns[i].teamID == turn) // loop through all towns and check if this town is apart of the team who is currently active
                {
                    manager.towns[i].DeclareStage();
                }
                else
                    manager.towns[i].unitsThisTurn = 0; //maybe add a declare not turn function if needs increase?
            }
        }
        barTimer = 0;
    }
    public int CurrentStage()

    {
        return stage;
    }
}
