using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TownObject : MonoBehaviour
{
    public TownManager.Town town;
    TMP_Text UI;
    public GameObject UIObj;
    // Start is called before the first frame update
    void Start()
    {
        UIObj = transform.GetChild(0).GetChild(0).gameObject;
        UI = UIObj.GetComponentInChildren<TMP_Text>();

        UIObj.SetActive(false);

        Camera cam = Camera.main;
        transform.GetChild(0).rotation = cam.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (town != null && UIObj.activeSelf)
        {
            if (town.unitsThisTurn <= 0)
            {
                UI.text = town.name + "\n Total Units: " + town.Units;
            }
            else
                UI.text = town.name + "\n Moveable Units: " + town.unitsThisTurn + "/" + town.Units;
        }
    }
}
