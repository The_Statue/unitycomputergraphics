using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TownDescriber : MonoBehaviour
{
    TownManager townManager;
    public TMP_Text name;
    public TMP_Text description;
    public TMP_Text units;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void selected(TownManager.Town selection)
    {
        if (selection != null)
        {
            name.text = selection.name;
            description.text = selection.description;
            units.text = "Units: " + selection.Units;
        }
        else
        {
            name.text = "Select a town!";
            description.text = "";
            units.text = "";
        }
    }
}
