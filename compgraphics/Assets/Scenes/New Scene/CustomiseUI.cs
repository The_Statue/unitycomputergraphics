using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CustomiseUI : MonoBehaviour
{
    public GenerateTexture manager;
    public TurnManager turnManager;
    bool first = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Redraw()
    {
        if (first)
        {
            manager.generatePoints();
            turnManager.IncrementTurn(); //tell the turn manager to change to turn 1
        }
        else
            manager.ReRenderImage();
        first = false;
    }

    public void Roads(TMP_Text UI)
    {
        bool temp = manager.drawRoads;
        manager.drawRoads = !temp;
        if (!temp)
            UI.text = "Hide Roads";
        else
            UI.text = "Show Roads";
    }

    public void Blur(TMP_Text UI)
    {
        bool temp = manager.drawBlur;
        manager.drawBlur = !temp;
        if (!temp)
            UI.text = "Hide Blur";
        else
            UI.text = "Show Blur";
    }

    public void Grass(TMP_Text UI)
    {
        bool temp = manager.drawGrass;
        manager.drawGrass = !temp;
        if (!temp)
            UI.text = "Hide Grass";
        else
            UI.text = "Show Grass";
    }

    public void Bold(TMP_Text UI)
    {
        bool temp = manager.drawBolden;
        manager.drawBolden = !temp;
        if (!temp)
            UI.text = "Unbolden Lines";
        else
            UI.text = "Bolden Lines";
    }

    public void Teams(TMP_Text UI)
    {
        bool temp = manager.drawTeams;
        manager.drawTeams = !temp;
        if (!temp)
            UI.text = "Show Towns";
        else
            UI.text = "Show Teams";
    }
}
